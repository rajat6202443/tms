
<!DOCTYPE html>
<html>
   <head>
	   <title>Task Management System</title>  
	   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	   <link rel="stylesheet" type="text/css" href="https://colorlib.com/polygon/adminty/files/assets/pages/j-pro/css/font-awesome.min.css">
   </head>
   <body>
    <?php include 'header.php';?>   
	
    <div class="container">
	    <h1 align="center">Task Management System</h1> 
		
	    <div class="panel panel-default">
			<div class="panel-heading">
			    <h3 class="panel-title">Task Details</h3>
			</div>
			<div class="panel-body">
				<div class="content-wrapper">
					<section class="content-header">
					    <span id="success_message"></span>						
					</section>				   
					<section class="content">					  
					    <div class="box">
							<div class="box-header with-border">							  
								<div class="row">
									<div class="col-sm-6">
									    <h3 class="box-title">Task</h3>
									</div>									
									<div class="col-sm-6 text-right">
									    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#add_task">
										    <i class="fa fa-plus"> Add Task</i>
									    </button>
									</div>
								</div>
							</div>
							<div class="box-body">
								<div class="overflow">
									<table class="table table-bordered">
										<thead>
										    <tr>
												<th>#</th>
												<th>Title</th>											
												<th>Due Date</th>												
												<th>Updated</th>
												<th>Status</th>
												<th>Action</th>
										    </tr>
										</thead>
										<tbody>
										  <?php 

										  $x = $offset+1;

										  foreach ($task as $rowVAL) { ?>
											<tr>
												<td><?php echo $x++; ?></td>									  
												<td><?php echo $rowVAL->title; ?></td>												
												<td><?php echo $rowVAL->due_date; ?></td>
												<td><?php echo $rowVAL->updated_on; ?></td>
												<td>											    
												    <?php if($rowVAL->status=='Pending'){ echo '<a class="btn btn-danger btn-xs"  data-toggle="tooltip">Pending'; }  ?>
                                                    <?php if($rowVAL->status=='Completed'){ echo '<a class="btn btn-success btn-xs"  data-toggle="tooltip">Completed'; }  ?>
												</td>																																																
												<td>
													<a class="btn btn-success btn-xs" onclick="getTaskDetails('<?php echo $rowVAL->task_id; ?>','view_task')" data-toggle='modal' data-target='#view_task' title="View"><i class="fa fa-eye"></i></a>
													<a class="btn btn-primary btn-xs" onclick="getTaskDetails('<?php echo $rowVAL->task_id; ?>','edit_task')" data-toggle='modal' data-target='#view_task' title="View" title="Edit"><i class="fa fa-edit"></i></a>
													<a class="btn btn-danger btn-xs" onclick="deleteUser(<?php echo $rowVAL->task_id; ?>)" data-toggle="tooltip" title="Delete"><i class="fa fa-trash"></i></a>
													<?php if($rowVAL->status=='Pending'){ $changestatus = "Completed"; ?> 
													    <a class="btn btn-success btn-xs" onclick="updateStatus(<?php echo $rowVAL->task_id; ?>,'<?php echo $changestatus; ?>')" data-toggle="tooltip" title="Update Completed"><i class="fa fa-check"></i></a>
													<?php } else if($rowVAL->status=='Completed'){ $changestatus = "Pending"; ?> 
													    <a class="btn btn-danger btn-xs" onclick="updateStatus(<?php echo $rowVAL->task_id; ?>,'<?php echo $changestatus; ?>')" data-toggle="tooltip" title="Update Pending"><i class="fa fa-ban"></i></a>
													<?php } ?>												
												</td>
											</tr>
										  <?php } ?>
										  
										</tbody>
									</table>
								</div>
								<div class="row">
									<div class="col-sm-6">
										Total Records : <?php echo $total; ?>
									</div>
									<div class="col-sm-6 text-right">
										<?php echo $links; ?>
									</div>
								</div>						  
							</div>						   
							<div class="box-footer">  </div>
						</div>					  
					</section>  
				</div>
            </div>
		</div>
    </div>
	
	<!-- ADD TASK Modal -->
	<div id="add_task" class="modal fade" role="dialog">
		<div class="modal-dialog">					
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Task</h4>
				</div>				
				<div class="modal-body">
					<form method="post" id="task_form">
					    <input type="hidden" name="form_name" id="form_name" readonly value="add" />
						<div class="form-group">
							<label>Title</label>
							<input type="text" name="title" id="title" class="form-control" />
							<span id="title_error" class="text-danger"></span>
						</div>
						<div class="form-group">
						    <label>Description</label>
						    <textarea type="text" name="description" id="description" class="form-control"/></textarea>
						    <span id="description_error" class="text-danger"></span>
						</div>
						<div class="form-group">
						    <label>Due Date</label>
						    <input type="date" name="due_date" id="due_date" class="form-control" />
						    <span id="due_date_error" class="text-danger"></span>
						</div>						
						
						<div class="form-group text-right">
							<button type="button" id="cancel" class="btn btn-default" data-dismiss="modal" >Close</button>
							<button type="submit"  id="save" class="btn btn-success" >Add</button>
						</div>
					</form>
					<div class="form-group" id="process" style="display:none;">
						<div class="progress">
						   <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style=""></div>
						</div>
					</div>
				</div>
				<div class="modal-footer"></div>
			</div>		
		</div>
	</div>
	
	<!-- EDIT TASK DETAILS Modal -->
	<div id="view_task" class="modal fade" role="dialog">
		<div class="modal-dialog">					
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" id="task_type"> Task Details</h4>
				</div>				
				<div class="modal-body">
					<form method="post" id="edit_task_form">
					    <input type="hidden" name="form_name" id="form_name" readonly value="edit" />
					    <input type="hidden" name="task_id" id="task_id" readonly class="form-control" />
						<div class="form-group">
							<label>Title</label>
							<input type="text" name="title" id="view_title" readonly class="form-control" />
                            <span id="edit_title_error" class="text-danger"></span>							
						</div>
						<div class="form-group">
						    <label>Description</label>
						    <textarea type="text" name="description" id="view_description" readonly class="form-control"/></textarea>
                            <span id="edit_description_error" class="text-danger"></span>						    
						</div>
						<div class="form-group">
						    <label>Due Date</label>
						    <input type="date" name="due_date" id="view_due_date" readonly class="form-control" />
                            <span id="edit_due_date_error" class="text-danger"></span>						    
						</div>											
						<div class="form-group text-right">
							<button type="button" id="edit_cancel" class="btn btn-default" data-dismiss="modal" >Close</button>
                            <button type="submit"  id="edit_save" disabled class="btn btn-success" >Save</button>							
						</div>
                    </form>
                    <div class="form-group" id="edit_process" style="display:none;">
						<div class="progress">
						   <div class="progress-bar progress-bar-striped active" id="progress-bar-edit" role="progressbar" aria-valuemin="0" aria-valuemax="100" style=""></div>
						</div>
					</div>					
				</div>
				<div class="modal-footer"></div>
			</div>		
		</div>
	</div>
	
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
 <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>	
<script>
    $(document).ready(function(){
  
        $('#task_form').on('submit', function(event)
		{
			var form = $('#form_name').val();
			//alert(form);
			event.preventDefault();
			var count_error = 0;

			if($('#title').val() == '')
			{
				$('#title_error').text('Title is required!');
				count_error++;
			}
			else
			{
				$('#title_error').text('');
			}

			if($('#description').val() == '')
			{
				$('#description_error').text('Description is required!');
				count_error++;
			}
			else
			{
				$('#description_error').text('');
			}
		   
			if($('#due_date').val() == '')
			{
				$('#due_date_error').text('Due date is required!');
				count_error++;
			}
			else
			{
				$('#due_date_error').text('');
			}

			if(count_error == 0)
			{
				
					$.ajax({
						url:"<?php echo base_url(); ?>Home/save_task",
						method:"POST",
						data:$(this).serialize(),
						beforeSend:function()
						{
						   $('#save').attr('disabled', 'disabled');
						   $('#cancel').css('disabled', 'disabled');
						   $('#process').css('display', 'block');
						   
						},
						success:function(data)
						{
							var percentage = 0;
							var timer = setInterval(function(){
							percentage = percentage + 20;
							progress_bar_process(percentage, timer, form);
							}, 300);
						}
					})
				
			}
			else
			{
				return false;
			}
	    });
		
		
		
		$('#edit_task_form').on('submit', function(event)
		{
			var form = "edit";
			//alert(form);
			event.preventDefault();
			var count_error = 0;

			if($('#view_title').val() == '')
			{
				$('#edit_title_error').text('Title is required!');
				count_error++;
			}
			else
			{
				$('#edit_title_error').text('');
			}

			if($('#view_description').val() == '')
			{
				$('#edit_description_error').text('Description is required!');
				count_error++;
			}
			else
			{
				$('#edit_description_error').text('');
			}
		   
			if($('#view_due_date').val() == '')
			{
				$('#edit_due_date_error').text('Due date is required!');
				count_error++;
			}
			else
			{
				$('#edit_due_date_error').text('');
			}

			if(count_error == 0)
			{
				//alert("ffafafa");
					$.ajax({
						url:"<?php echo base_url(); ?>Home/update_task",
						method:"POST",
						data:$(this).serialize(),
						beforeSend:function()
						{
						   $('#edit_save').attr('disabled', 'disabled');
						   $('#edit_cancel').css('disabled', 'disabled');
						   $('#edit_process').css('display', 'block');						   
						},
						success:function(data)
						{
							//alert(data);
							var percentage = 0;
							var timer = setInterval(function(){
							percentage = percentage + 20;
							progress_bar_process(percentage, timer, form);
							}, 300);
						}
					})
				
			}
			else
			{
				return false;
			}
	    });

	    function progress_bar_process(percentage, timer, form)
	    {
			//alert(form);
			if(form=='add')
			{
				$('.progress-bar').css('width', percentage + '%');
				if(percentage > 100)
				{   			        				
					clearInterval(timer);
					$('#task_form')[0].reset();
					$('#process').css('display', 'none');
					$('.progress-bar').css('width', '0%');
					$('#save').attr('disabled', false);
					$('#cancel').attr('disabled', false);				
					$('#add_task').click(); 
					$('#success_message').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>×</button> Data Saved!</div>");
					setTimeout(function(){
					    $('#success_message').html('');
					    location.reload(true);
					}, 4000);				
				}
			}
			else if(form=='edit')
			{
				$('.progress-bar').css('width', percentage + '%');
				if(percentage > 100)
				{   			        				
					clearInterval(timer);
					$('#edit_task_form')[0].reset();
					$('#edit_process').css('display', 'none');
					$('.progress-bar').css('width', '0%');
					$('#edit_save').attr('disabled', false);
					$('#edit_cancel').attr('disabled', false);				
					$('#view_task').click(); 
					$('#success_message').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>×</button> Data Updated!</div>");
					setTimeout(function(){
					    $('#success_message').html('');
					    location.reload(true);
					}, 4000);				
				}
			}
	    }

    });
</script>

<script>

    function getTaskDetails(task_id,task) 
	{

		if(task=='view_task')
		{ 
		    $('#task_type').html('Task Details');
			$('#edit_save').html('Save');
			$.ajax(
				{     
					url: "<?php  echo base_url(); ?>Home/task_details",
					method:"POST", 
					data:{task_id:task_id},
					success: function(data) 
					{  
						//alert(data); 
						if (data != null) 
						{							
                            $('#task_id').val(data.task_id);
 							$('#edit_save').attr('disabled', true);
							
							$('#view_title').val(data.title);
							$('#view_title').attr('readonly', true);
							
							$('#view_description').val(data.description);
							$('#view_description').attr('readonly', true);
							
							$('#view_due_date').val(data.due_date);
							$('#view_due_date').attr('readonly', true);
						}
					}
				})
		}
		else if(task=='edit_task')
		{ 
		    $('#task_type').html('Edit Task');
			$('#edit_save').html('Update');
			$.ajax(
				{     
					url: "<?php  echo base_url(); ?>Home/task_details",
					method:"POST", 
					data:{task_id:task_id},
					success: function(data) 
					{  
						//alert(data); 
						if (data != null) 
						{
							
                            $('#task_id').val(data.task_id);
 							$('#edit_save').attr('disabled', false);
							
							$('#view_title').val(data.title);
							$('#view_title').attr('readonly', false);
							
							$('#view_description').val(data.description);
							$('#view_description').attr('readonly', false);
							
							$('#view_due_date').val(data.due_date);
							$('#view_due_date').attr('readonly', false);
						}
					}
				})
		}			 
	}
	
	
</script>

<script>
	function deleteUser(task_id) 
	{
		var txt;
		var r = confirm("Are you Sure ? ");
		if (r == true) 
		{			
			$.ajax(
				{     
					url: "<?php  echo base_url(); ?>Home/deleteTask",
					method:"POST", 
					data:{task_id:task_id},
					success: function(data) 
					{  
						//alert(data); 
						if (data != null) 
						{							
                            $('#success_message').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>×</button> Task Deleted!</div>");
							setTimeout(function(){
							 $('#success_message').html('');
							 location.reload(true);
							}, 4000);
						}
						else 
						{							
                            $('#success_message').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>×</button> Task NotDeleted!</div>");
							setTimeout(function(){
							 $('#success_message').html('');
							 location.reload(true);
							}, 4000);
						}
					}
				})
		} 
	}
</script>

<script>
	function updateStatus(task_id,status) 
	{
		var txt;
		var r = confirm("Are you Sure ? ");
		if (r == true) 
		{			
			$.ajax(
				{     
					url: "<?php  echo base_url(); ?>Home/updateStatus",
					method:"POST", 
					data:{task_id:task_id,status:status},
					success: function(data) 
					{  
						//alert(data); 
						if (data != null) 
						{							
                            $('#success_message').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>×</button> Task Status Updated!</div>");
							setTimeout(function(){
							 $('#success_message').html('');
							 location.reload(true);
							}, 4000);
						}
						else 
						{							
                            $('#success_message').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>×</button> Task Status Not Updated!</div>");
							setTimeout(function(){
							 $('#success_message').html('');
							 location.reload(true);
							}, 4000);
						}
					}
				})
		} 
	}
</script>
	
 </body>
</html>