<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html><head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Task Management System</title>
     
	<link rel="icon" href="<?php echo base_url(); ?>assets/dist/favicon.ico?1672204482497">
    <link href="<?php echo base_url(); ?>assets/dist/css/chunk-vendors.css?1672204482497" rel="preload" as="style">
    <link href="<?php echo base_url(); ?>assets/dist/css/app.css?1672204482497" rel="preload" as="style">
    <link href="<?php echo base_url(); ?>assets/dist/js/chunk-vendors.js?1672204482497" rel="preload" as="script">
    <link href="<?php echo base_url(); ?>assets/dist/js/app.js?1672204482497" rel="preload" as="script">
    <link href="<?php echo base_url(); ?>assets/dist/css/chunk-vendors.css?1672204482497" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/dist/css/app.css?1672204482497" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://colorlib.com/polygon/adminty/files/assets/icon/feather/css/feather.css">
</head>
<body>
    <style>
    :root {
            --oxd-primary-one-color:#19d3d0;
            --oxd-primary-font-color:#FFFFFF;
            --oxd-secondary-four-color:#76BC21;
            --oxd-secondary-font-color:#FFFFFF;
            --oxd-primary-gradient-start-color:#FF920B;
            --oxd-primary-gradient-end-color:#F35C17;
            --oxd-secondary-gradient-start-color:#FF920B;
            --oxd-secondary-gradient-end-color:#F35C17;
            --oxd-primary-one-lighten-5-color:#21e5e2;
            --oxd-primary-one-lighten-30-color:#93f2f1;
            --oxd-primary-one-darken-5-color:#16bcba;
            --oxd-primary-one-alpha-10-color:rgba(25, 211, 208, 0.1);
            --oxd-primary-one-alpha-15-color:rgba(25, 211, 208, 0.15);
            --oxd-primary-one-alpha-20-color:rgba(25, 211, 208, 0.2);
            --oxd-primary-one-alpha-50-color:rgba(25, 211, 208, 0.5);
            --oxd-secondary-four-lighten-5-color:#84d225;
            --oxd-secondary-four-darken-5-color:#68a61d;
            --oxd-secondary-four-alpha-10-color:rgba(118, 188, 33, 0.1);
            --oxd-secondary-four-alpha-15-color:rgba(118, 188, 33, 0.15);
            --oxd-secondary-four-alpha-20-color:rgba(118, 188, 33, 0.2);
            --oxd-secondary-four-alpha-50-color:rgba(118, 188, 33, 0.5);
        }
		
    </style>
    
    <div id="app" data-v-app="">
	    <div class="orangehrm-login-layout" data-v-50815349="" data-v-358db50f="">
	        <div class="orangehrm-login-layout-blob" data-v-50815349="">
			    <div class="orangehrm-login-container" data-v-50815349="">
				    <div class="orangehrm-login-slot-wrapper" data-v-50815349="">
					    <div class="orangehrm-login-branding" data-v-3dda64e6="" data-v-50815349="">
						    <h5 class="oxd-text oxd-text--h5 orangehrm-login-title" data-v-7b563373="" data-v-358db50f=""><span style="margin-top:10px">TASK MANAGEMENT SYSTEM </span></h5>
					    </div><br data-v-358db50f=""><br data-v-358db50f="">
						<div class="orangehrm-login-slot" data-v-50815349="">						    
							<div class="orangehrm-login-logo-mobile" data-v-50815349="">
							    <h5 class="oxd-text oxd-text--h5 orangehrm-login-title" data-v-7b563373="" data-v-358db50f="">TASK MANAGEMENT SYSTEM</h5>
							</div>
							<!--<h5 class="oxd-text oxd-text--h5 orangehrm-login-title" data-v-7b563373="" data-v-358db50f="">Login</h5>-->
							<div class="orangehrm-login-form" data-v-358db50f="">
							    <?php if(!empty($this->session->flashdata('feedback'))) {  ?>
							    <div id="mydivs" class="oxd-alert oxd-alert--error" role="alert" data-v-0b423d90="" data-v-30b9e6c4="" style="color: #eb0910;background-color: rgba(235,9,16,.06);justify-content: space-between;min-height: 60px;border-radius: 1.2rem;margin: 1rem 0;font-size: 14px;font-weight: 600;">
									<div class="oxd-alert-content oxd-alert-content--error" data-v-0b423d90="" style="display: -webkit-box;display: -webkit-flex;display: -moz-box;display: flex;-webkit-box-align: center;-webkit-align-items: center;-moz-box-align: center;align-items: center;position: relative;padding: 0rem;font-size: inherit;">
										<i class="oxd-icon bi-exclamation-circle oxd-alert-content-icon" data-v-013b3fcc="" data-v-0b423d90="" style="margin-right: 0rem;font-size: 1.5rem;color: inherit;    padding: 1rem;"></i>
									    <p class="oxd-text oxd-text--p oxd-alert-content-text" data-v-7588b244="" data-v-0b423d90="" style="font-family: Nunito Sans;margin-top: 0;margin-bottom: 0; margin-inline-start: 0px;margin-inline-end: 0px;"><?php echo $this->session->flashdata('feedback'); ?></p>
								    </div>
								</div>
								<?php } ?>  
							    
								<form class="oxd-form"  method="post" action="<?php echo base_url(); ?>Login/validate" data-v-d5bfe35b="" data-v-358db50f=""><!---->
									<input name="_token" type="hidden" value="97c313eb341af9696.kClKCS-0DYkhF8vDRt-iCoqsTKs9ZHpeopgAU75LIdA.-EsdaFbjfshMSPyBBLDJOunVdPl2Fiwc0ew0Nf0qTb_IfhA-TudXpEpEoA" data-v-358db50f="">
									<div class="oxd-form-row" data-v-2130bd2a="" data-v-358db50f="">
									    <div class="oxd-input-group oxd-input-field-bottom-space" data-v-957b4417="" data-v-358db50f="">
										    <div class="oxd-input-group__label-wrapper" data-v-957b4417="">
												<i class="oxd-icon bi-person oxd-input-group__label-icon" data-v-bddebfba="" data-v-957b4417=""></i>
												<label class="oxd-label" data-v-30ff22b1="" data-v-957b4417="">Username</label>
											</div>
											<div class="" data-v-957b4417="">
											    <input class="oxd-input oxd-input--active" name="username" placeholder="Username" autofocus="" data-v-1f99f73c="">
											</div>
									    </div>
									</div>
									<div class="oxd-form-row" data-v-2130bd2a="" data-v-358db50f="">
									    <div class="oxd-input-group oxd-input-field-bottom-space" data-v-957b4417="" data-v-358db50f="">
											<div class="oxd-input-group__label-wrapper" data-v-957b4417="">
												<i class="oxd-icon bi-key oxd-input-group__label-icon" data-v-bddebfba="" data-v-957b4417=""></i>
												<label class="oxd-label" data-v-30ff22b1="" data-v-957b4417="">Password</label>
											</div>	
											<div class="" data-v-957b4417="">
											    <input class="oxd-input oxd-input--active" type="password" name="password" placeholder="Password" data-v-1f99f73c="">
											</div>
										</div>
									</div>
																	
								    <div class="oxd-form-actions orangehrm-login-action" data-v-19c2496b="" data-v-358db50f="">
										<button type="submit" class="oxd-button oxd-button--medium oxd-button--main orangehrm-login-button" data-v-10d463b7="" data-v-358db50f="" style="font-weight: 1000;"><i class="feather icon-log-in" style="font-weight: 1000;"></i><!----> Login <!----></button>
									</div>
									<div class="orangehrm-login-forgot" data-v-358db50f="">
									    <p class="oxd-text oxd-text--p orangehrm-login-forgot-header" data-v-7b563373="" data-v-358db50f="">Forgot your password? </p>
									</div>
							    </form>
								<br data-v-358db50f="">
							</div>														
						</div>
					</div>
				</div>
				<div class="orangehrm-login-logo" data-v-50815349="">
				    <img src="<?php echo base_url(); ?>assets/dist/images/technical8.png" alt="orangehrm-logo" data-v-50815349="">
				</div>
			</div>
		</div>
		
	</div>
	
    	
    <script src="<?php echo base_url(); ?>assets/dist/js/chunk-vendors.js?v=1672204482497"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/app.js?v=1672204482497"></script>
	<script src="<?php echo base_url(); ?>assets/dist/js/sweetalert2.all.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
	
	<script>
	   
        setTimeout(function() {
            $('#mydivs').hide('fast');
        }, 3000);
		
    </script>
	
</body></html>