<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
		parent::__construct(); 
		
		$this->load->library('form_validation');
		$this->load->model('Login_model');
		$this->load->model('Admin_model');		
		$this->load->helper('url', 'form');
	    $this->load->helper('form'); 
    }


    public function index()
    {     		
        $this->load->view('login.php');      		
    }
		
	public function validate()
    {
		       
        if ($this->input->server('REQUEST_METHOD') == 'GET') 
		{
            $this->output->set_status_header(405);
        } 
		else 
		{
			                    
            $this->form_validation->set_rules('username', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');                      
            $password = hash_hmac("sha256", $this->input->post("password"), 'aSm0$i_20eNh3os');			
		            
            if ($this->form_validation->run() === false) 
			{
				
				$this->session->set_flashdata("feedback","Provide Complete Details!");                
				$this->index();
                
            } 
			else 
			{ 
		      				
				$row = $this->Login_model->getUserForLogin('users',$password);
				
				 				
				if($row)
				{	
					$this->session->set_userdata("user_login", "1");
					$this->session->set_userdata("user_login_id", $row['user_id']);
					$this->session->set_userdata("username", $row['user_name']);					
					
					$this->session->set_flashdata("login_status","User Login Succcessfully");
					$this->session->set_flashdata("status_icon", "success");
					$this->session->set_flashdata("status", "Login Succcessful");

					return redirect('Home/Task');
				}
				else
				{
					$this->session->set_flashdata("feedback","User id or Password is Invalid!");
					//$data['error']= 'User id or Password is Invalid!';					
					$this->index();
				}  
			}				         
        }
    }
	
	public function logout(){

		$this->session->sess_destroy();
		redirect('Login');

	}
	
 
	
              		
    }





             



