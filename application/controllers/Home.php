<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
    function __construct()
    {

        parent::__construct();
        $this->load->database();        
        $this->load->library('session');
        $this->load->model('Admin_model');
        $this->load->library('pagination');

    }
	
	
	public function checkLogin()
	{
		if($this->session->userdata['user_login']=='1')
		{
			return "true";
		}
		else
		{
			redirect('Login');
		}
	}
	
	public function Task()
	{
		
		$this->checkLogin();
        date_default_timezone_set('Asia/Kolkata');      
        $create_time = date('Y-m-d H:i:s', time());
		
		$offset=0;
		$limit = 4;

		if ($this->input->get('per_page')) 
		{
			$offset=$this->input->get('per_page');
		}
		$data['offset']=$offset;

		$task_data = $this->Admin_model->task_list($limit,$offset);

		$data['total'] = $task_data['total'];
		$param = array();

		$config['base_url'] = base_url()."Home/Task".'?'.http_build_query($param);
		$config['total_rows'] = $task_data['total'];
		$config['per_page'] = $limit;
		$config['enable_query_strings'] = true;
		$config['page_query_string'] = true;
		$config['full_tag_open'] = '<ul class="pagination pagination-small pagination-centered">';
	    $config['full_tag_close'] = '</ul>';
	    $config['num_links'] = 5;
	    $config['prev_link'] = '&lt; Prev';
	    $config['prev_tag_open'] = '<li>';
	    $config['prev_tag_close'] = '</li>';
	    $config['next_link'] = 'Next &gt;';
	    $config['next_tag_open'] = '<li>';
	    $config['next_tag_close'] = '</li>';
	    $config['cur_tag_open'] = '<li class="active"><a href="#">';
	    $config['cur_tag_close'] = '</a></li>';
	    $config['num_tag_open'] = '<li>';
	    $config['num_tag_close'] = '</li>';
	    $config['first_tag_open'] = '<li>';
	    $config['first_tag_close'] = '</li>';
	    $config['last_tag_open'] = '<li>';
	    $config['last_tag_close'] = '</li>';
	    

		$this->pagination->initialize($config);
		$data['links'] = $this->pagination->create_links();
		$data['task'] = $task_data['task'];
		
		$this->load->view('task_list',$data);

	}


    public function add_task()
	{
		$this->checkLogin();
		$this->load->view('create_task');
	}
	
	public function save_task()
	{

		$this->checkLogin();
        date_default_timezone_set('Asia/Kolkata');      
        		
		$data['title'] = $this->input->post('title');
		$data['description'] = $this->input->post('description');
		$data['due_date'] = $this->input->post('due_date');
		
		$data['created_on'] = date('Y-m-d H:i:s', time());
        $data['updated_on'] = date('Y-m-d H:i:s', time());
		
		echo $insert_id =  $this->Admin_model->addRecords('task',$data);
        
	}

	public function task_details()
	{

		$this->checkLogin();
        $task_id = $this->input->post('task_id');
		
		$task_details = $this->Admin_model->get_details('task',$task_id);
		                                              
        $response = array(
                    'task_id' => $task_details->task_id,
                    'title' => $task_details->title,
                    'description' => $task_details->description,
                    'due_date' => $task_details->due_date,
                    'status' => $task_details->status,							
                    );
		
        header('Content-Type: application/json');
        echo json_encode($response);
	}
	
	public function deleteTask()
	{
        
		$this->checkLogin();
		$task_id = $this->input->post('task_id');
		echo $task =  $this->Admin_model->delete_task('task',array('task_id'=>$task_id));		

	}
	
	public function update_task()
	{

		$this->checkLogin();
        date_default_timezone_set('Asia/Kolkata');
		
        $task_id = $this->input->post('task_id');	
		$data['title'] = $this->input->post('title');
		$data['description'] = $this->input->post('description');
		$data['due_date'] = $this->input->post('due_date');		
        $data['updated_on'] = date('Y-m-d H:i:s', time());
		
		echo $response =  $this->Admin_model->updateRecords('task',$data,'task_id='.$task_id);
        
	}
	
	public function updateStatus()
	{

		$this->checkLogin();
        date_default_timezone_set('Asia/Kolkata');
		
        $task_id = $this->input->post('task_id');	
		$data['status'] = $this->input->post('status');		
        $data['updated_on'] = date('Y-m-d H:i:s', time());
		
		echo $response =  $this->Admin_model->updateRecords('task',$data,'task_id='.$task_id);
        
	}
	
	
}
