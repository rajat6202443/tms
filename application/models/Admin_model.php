<?php

class admin_model extends CI_Model{

	function __consturct()
	{
	    parent::__construct();	
	}
	
		
	public function task_list($limit,$offset,$param=array())
	{

		$this->db->select('task.*')->from('task') ;       
        $total_result =  $this->db->count_all_results();

		$this->db->select('task.*')->from('task');        
        $this->db->limit($limit,$offset);
        $this->db->order_by('task_id', 'DESC');
        $task =  $this->db->get()->result();

        return array('total'=>$total_result,'task'=>$task);

	}
	
	function addRecords($tblName, $data)
	{	
	
		$this-> db->insert($tblName, $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
		
	}
	
	public function get_details($tblName,$id) 
	{

		$this->db->select('*');						
		$this->db->where('task_id ', $id);					
		$query = $this->db->get($tblName);			
		return $query->row();
		
	}


    public function delete_task($tblName, $task_id)
	{
		
		$this->db->where($task_id);		
		$this->db->delete($tblName);   	
		return $this->db->affected_rows();
		//print_r($this->db->last_query());
		
	}
	
	
	public function	updateRecords($tblName, $data, $clause)
	{
		
		$this->db->set($data);			
		$this->db->where($clause);			
		$this->db->update($tblName);			
		return $this->db->affected_rows();
		//print_r($this->db->last_query());
		
	}




    }


